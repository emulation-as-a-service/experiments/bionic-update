from phusion/baseimage:0.11

run apt-get update && apt-get upgrade -y

run apt-get install -y git build-essential 

run git clone https://github.com/eaas-framework/xmount.git -b debian/bwfla

## xmount dependencies

run apt-get install -y cdbs quilt cmake pkg-config libfuse-dev zlib1g-dev libewf-dev libssl-dev

workdir /xmount/trunk
run dpkg-buildpackage -b -us -uc
run dpkg -i ../xmount_0.7.4bwfla6_amd64.deb
workdir /

run git clone https://github.com/eaas-framework/qemu.git -b libxmount

add control /qemu/debian/control

# qemu dependencies
run apt-get install -y \ 
debhelper device-tree-compiler texinfo python acpica-tools libaio-dev libasound2-dev \
libattr1-dev libbluetooth-dev libbrlapi-dev libcap-dev libcap-ng-dev libcurl4-openssl-dev libfdt-dev gnutls-dev libncurses5-dev libpixman-1-dev libpulse-dev librados-dev librbd-dev libsasl2-dev libsdl1.2-dev \
libspice-server-dev libspice-protocol-dev libusb-1.0-0-dev libusbredirparser-dev libvdeplug-dev libxen-dev uuid-dev xfslibs-dev libjpeg-dev libbz2-dev liblzo2-dev libsnappy-dev \
libewf-dev libpng-dev

workdir /qemu
run dpkg-buildpackage -b -us -uc -d


# guacd deps
run apt-get install -y \
libcairo-dev libcunit1-dev libfreerdp-dev libvorbis-dev libvncserver-dev libpango1.0-dev libossp-uuid-dev

workdir / 

run git clone https://gitlab.com/emulation-as-a-service/guacamole-server-eaas.git -b debian
workdir /guacamole-server-eaas
run dpkg-buildpackage -b -uc -us 

workdir /
run dpkg -i libguac7_0.9.5-1bwfla7_amd64.deb
run dpkg -i libguac-client-sdlonp0_0.9.5-1bwfla7_amd64.deb
run dpkg -i libguac-dev_0.9.5-1bwfla7_amd64.deb

run apt-get install -y \ 
libxt-dev libxv-dev libaudiofile-dev libxkbcommon-dev
run git clone https://gitlab.com/emulation-as-a-service/libsdl-eaas.git -b debian
workdir /libsdl-eaas
run dpkg-buildpackage -b -uc -us 

workdir /
run mkdir /debs
run cp /xmount/xmount_0.7.4bwfla6_amd64.deb /debs
run cp *.deb /debs

